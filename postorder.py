def postOrderNonRecursive(root,result):
	if not root:
		return
	stack = []
	node = root
	while stack or node:
		if node:
			if node.right:
				stack.append(node.right)
			if node.left:	
				stack.append(node.left)
			node = node.left	
		# elif node.left == None and node.right == None:
		# 	result.append(node)	
		else:
			node = stack.pop()

			result.append(node)
			node = node.right				
