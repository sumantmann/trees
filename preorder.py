class BinaryTree:
	def __init__(self):
		self.data = None
		self.left = None
		self.right = None
	
	def setLeft(self,data):
		newNode = BinaryTree()
		newNode.data = data
		self.left = newNode
	
	def setRight(self,data):
		newNode = BinaryTree()
		newNode.data = data
		self.right = newNode
	
	def preOrderRecursive(self,root,result):
		if not root:
			return
		result.append(root.data)
		#print(result)
		self.preOrderRecursive(root.left,result)
		self.preOrderRecursive(root.right,result)

	def preOrderNonRecursive(self,root,result):
		if not root:
			return
		stack = []
		stack.append(root)
		#result.append(root.data)
		while stack:
			node = stack.pop()
			result.append(node.data)
			if node.right:
				stack.append(node.right)
			if node.left:
				stack.append(node.left)
		return result		


binary_tree = BinaryTree()
binary_tree.data = 1
binary_tree.setRight(3)	
binary_tree.setLeft(2)
binary_tree.left.setLeft(4)
binary_tree.left.setRight(5)
binary_tree.right.setLeft(6)
binary_tree.right.setRight(7)

#recursive calling.............
#result = []	
#binary_tree.preOrderRecursive(binary_tree,result)
#print(result)	

#non recursive calling............
print(binary_tree.preOrderNonRecursive(binary_tree,[]))

