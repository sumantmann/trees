class BinaryTree:
	def __init__(self):
		self.data = None
		self.left = None
		self.right = None
	
	def setLeft(self,data):
		newNode = BinaryTree()
		newNode.data = data
		self.left = newNode
	
	def setRight(self,data):
		newNode = BinaryTree()
		newNode.data = data
		self.right = newNode

	def inOrderRecursive(self,root,result):
		if not root:
			return
		inOrderRecursive(root.left,result)
		result.append(root.data)
		inOrderRecursive(root.right,result)

	def inOrderNonRecursive(self,root,result):
		if not root:
			return 
		stack = []
		node = root
		while stack or node:
			if node:
				stack.append(node)
				node = node.left
			else:
				node = stack.pop()
				result.append(node.data)
				node = node.right	

			